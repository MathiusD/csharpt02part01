# CSharpT02Part01

### About

Partie 01 du TP02 du module M2104 de Samy Delahaye
Ceci est une solution Visual Studio avec une approche TDD du TP en question
Note : Mon pseudo est renseigné dans les dépôts git et non pas mon nom et prénom

### Author

Mathieu Féry (Alias Mathius)