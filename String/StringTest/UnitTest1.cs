﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using String;

namespace StringTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestRenvoi()
        {
            Assert.AreEqual("Les framboises sont perchees sur le tabouret de mon grand-pere.", Program.Renvoi("Les framboises sont perchees sur le tabouret de mon grand-pere."));
            Assert.AreEqual("24zaertyuiop)^=0584", Program.Renvoi("24zaertyuiop)^=0584"));
            Assert.AreEqual("", Program.Renvoi(""));
        }
        [TestMethod]
        public void TestCasseInitiale()
        {
            Assert.AreEqual("Cette phrase commence par une majuscule.", Program.CasseInitiale("Les framboises sont perchees sur le tabouret de mon grand-pere."));
            Assert.AreEqual("Cette phrase commence par une minuscule.", Program.CasseInitiale("les framboises sont perchees sur le tabouret de mon grand-pere."));
            Assert.AreEqual("Cette phrase ne commence ni par une majuscule ou une minuscule.", Program.CasseInitiale("24zaertyuiop)^=0584"));
            Assert.AreEqual("Aucune données fournies.", Program.CasseInitiale(""));
        }
        [TestMethod]
        public void TestTypeFin()
        {
            Assert.AreEqual("Cette phrase se termine par un point.", Program.TypeFin("Les framboises sont perchees sur le tabouret de mon grand-pere."));
            Assert.AreEqual("Cette phrase ne se termine pas par un point.", Program.TypeFin("Les framboises sont perchees sur le tabouret de mon grand-pere"));
            Assert.AreEqual("Cette phrase ne se termine pas par un point.", Program.TypeFin("24zaertyuiop)^=0584"));
            Assert.AreEqual("Aucune données fournies.", Program.TypeFin(""));
        }
    }
}
